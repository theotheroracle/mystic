CREATE USER 'mystic'@'%'   IDENTIFIED BY '<SECURE MYSQL PASSWORD HERE>';
CREATE USER 'grimoire'@'%' IDENTIFIED BY '<MORDREDS SECURE MYSQL PASSWORD HERE>';

CREATE DATABASE mystic;
CREATE DATABASE mystic_testing;
CREATE DATABASE grimoire;

GRANT ALL ON mystic.*   TO 'mystic'@'%';
GRANT ALL ON mystic_testing.*   TO 'mystic'@'%';
GRANT ALL ON grimoire.* TO 'grimoire'@'%';
