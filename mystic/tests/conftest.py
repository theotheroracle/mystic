from pathlib import Path
from flask.app import Flask
from pytest import fixture, TempPathFactory
import sqlite3
from sqlite3 import Connection

from mystic import create_app, setup_database

from typing import Generator

@fixture(scope='session')
def data_path(tmp_path_factory: TempPathFactory) -> Path:
	return tmp_path_factory.mktemp('data')

@fixture(scope='session')
def database_connection(data_path: Path) -> Generator[Connection, None, None]:
	database_path = data_path / 'database.db'
	with sqlite3.connect(database_path) as database:
		yield database

@fixture(scope='session')
def app(data_path: Path, database_connection: Connection) -> Flask:
	c = database_connection.cursor()
	setup_database(c)
	database_connection.commit()
	c.close()
	return create_app({
		"SECRET_KEY": "KEY FOR TESTING ONLY",
		"DATABASE": data_path / 'database.db',
		"PROJECTS_FILE": data_path / 'projects.json'
	})
