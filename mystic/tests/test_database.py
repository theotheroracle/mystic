from __future__ import annotations
from mystic.sources import Git, GitHub, Source

from os import environ
from pymysql import connect
from pymysql.connections import Connection
from pymysql.cursors import Cursor
from pytest import fixture, raises

from typing import Callable, Iterator, List, Optional, Tuple, cast

from mystic.database import Project, MalformedId, NonexistantId, User, compute_alphaid, parse_alphaid
import mystic

@fixture(scope="session")
def database_connection() -> Iterator[Connection[Cursor]]:
	db = connect(
		host = environ.get('MYSTIC_DB_HOST', 'localhost'),
		user = 'mystic',
		password = '<SECURE MYSQL PASSWORD HERE>',
		charset='utf8mb4',
		database='mystic_testing',
		cursorclass=Cursor,
	)
	c = db.cursor()
	mystic.setup_database(c)
	db.commit()
	c.close()
	yield db

@fixture
def database(database_connection: Connection[Cursor]) -> Iterator[Cursor]:
	c = database_connection.cursor()
	yield c
	c.close()

@fixture
def projects(database: Cursor) -> Iterator[Callable[[str, str], Project]]:
	ids: List[int] = []
	def new_project(name: str, description: str = "A really neat project") -> Project:
		database.execute("""
			INSERT INTO projects(display_name, description) VALUES (%s, %s);
		""", (name, description))
		pid = database.lastrowid
		ids.append(pid)
		return Project(pid)
	yield new_project
	ids_template = ', '.join(['%s'] * len(ids))
	database.execute(f'DELETE FROM projects WHERE project_id IN ({ids_template});', ids)

@fixture
def project(projects: Callable[[str], Project]) -> Iterator[Project]:
	yield projects("MyProject")

@fixture
def users(database: Cursor) -> Iterator[Callable[[str], User]]:
	ids: List[int] = []
	def new_user(
			username: str,
			first: Optional[str] = None,
			last: Optional[str] = None,
			email: Optional[str] = None,
	) -> User:
		if first is None:
			first = username
		if last is None:
			last = first[::-1]
		if email is None:
			email = username + '@rit.edu'
		database.execute("""
			INSERT INTO users(username, first_name, last_name, email) VALUES (%s, %s, %s, %s);
		""", (username, first, last, email))
		uid = database.lastrowid
		ids.append(uid)
		return User(uid)
	yield new_user
	ids_template = ', '.join(['%s'] * len(ids))
	database.execute(f'DELETE FROM users WHERE user_id IN ({ids_template});', ids)

@fixture
def user(users: Callable[[str], User]) -> Iterator[User]:
	yield users("Emi")

@fixture
def missing_user() -> User:
	return User(9999)

@fixture
def missing_project() -> Project:
	return Project(9999)

@fixture
def establish_ownership(database: Cursor) -> Callable[[User, Project], None]:
	def output_function(user: User, project: Project) -> None:
		database.execute("""
			INSERT INTO owners VALUES (%s, %s);
		""", (user._user_id, project._project_id))
	return output_function

@fixture
def generate_source(database: Cursor) -> Callable[[Project, Tuple[str, str]], int]:
	def output_function(project: Project, values: Tuple[str, str]) -> int:
		database.execute("""
			INSERT INTO data_sources(project_id, data_type, data_url) VALUES (%s, %s, %s);
		""", (project._project_id, values[0], values[1]))
		return cast(int, database.lastrowid)
	return output_function

class TestProject:

	def test_project_constructor(self) -> None:
		p = Project(1)

		# Uninitialized variables
		p._project_id == 1
		p._display_name == None
		p._owners == None
		p._data_sources == None

	def test_instantiate_invalid_project_id(self) -> None:
		with raises(MalformedId):
			Project(-1)
		with raises(MalformedId):
			Project(999999999999999999999999)

	def test_load_project(self, database: Cursor, project: Project) -> None:
		project.load(database)
		assert project._display_name == "MyProject"

	def test_implicit_load_project(self, database: Cursor, project: Project) -> None:
		assert project.get_display_name(database) == "MyProject"

	def test_load_missing_project(self, database: Cursor, missing_project: Project) -> None:
		with raises(NonexistantId):
			missing_project.load(database)

	def test_add_owner(self, project: Project, user: User, database: Cursor) -> None:
		project.add_owner(database, user)

		assert project.get_owners(database) == [user]
		assert user.get_projects(database) == [project]

		database.execute("""
			SELECT * FROM owners;
		""")
		records = database.fetchall()
		assert len(records) == 1
		assert records[0] == (user.user_id, project.project_id)

	def test_add_owner_validates_missing_ids(
		self,
		database: Cursor,
		project: Project,
		missing_project: Project,
		user: User,
		missing_user: User
	) -> None:
		with raises(NonexistantId):
			project.add_owner(database, missing_user)

		with raises(NonexistantId):
			missing_project.add_owner(database, user)

	def test_adding_owner_updates_model(self, database: Cursor, project: Project, user: User) -> None:
		# Manually fetching owner and projects forces a cache of the database
		assert project.get_owners(database) == []
		assert user.get_projects(database) == []

		project.add_owner(database, user)

		# The cache should be updated to represent the new changes to the model
		assert project.get_owners(database) == [user]
		assert user.get_projects(database) == [project]

	def test_add_datasource(self, project: Project, database: Cursor) -> None:
		src_type = "github"
		src_url = "https://github.com/chaoss/grimoirelab"
		src_id = project.add_data_source(database, src_type, src_url)

		assert project.get_data_sources(database) == [Source(src_id, project, src_type, src_url)]

		database.execute("""
			SELECT * FROM data_sources;
		""")
		records = database.fetchall()
		assert len(records) == 1
		assert records[0] == (src_id, project.project_id, src_type, src_url)

	def test_add_datasource_validates_project(self, database: Cursor, missing_project: Project) -> None:
		with raises(NonexistantId):
			missing_project.add_data_source(database, "doesn't", "matter")

	def test_create_project(self, database: Cursor) -> None:
		p = Project.create_project(database, "CoolNewProject", "A really neat project")

		database.execute("""
			SELECT * FROM projects WHERE project_id = %s;
		""", (p.project_id,))
		assert database.fetchone() == (p.project_id, "CoolNewProject", "A really neat project", None)

		database.execute("""
			DELETE FROM projects WHERE project_id = %s;
		""", (p._project_id,))

	def test_project_retrieve_owners(
		self,
		database: Cursor,
		users: Callable[[str], User],
		project: Project,
		establish_ownership: Callable[[User, Project], None]
	) -> None:
		user1 = users("Emi")
		user2 = users("Bit")
		users("Unrelated User")
		establish_ownership(user1, project)
		establish_ownership(user2, project)

		assert len(project.get_owners(database)) == 2
		assert user1 in project.get_owners(database)
		assert user2 in project.get_owners(database)

	def test_project_retrieve_no_owners(
		self,
		database: Cursor,
		project: Project,
	) -> None:
		assert len(project.get_owners(database)) == 0

	def test_project_retrieve_datasources(
		self,
		database: Cursor,
		projects: Callable[[str], Project],
		generate_source: Callable[[Project, Tuple[str, str]], int],
	) -> None:
		project1 = projects("Oak")
		project2 = projects("Maple")
		project3 = projects("Maple")
		sources = [
			generate_source(project1, ('git', 'gitlink1')),
			generate_source(project1, ('git', 'gitlink2')),
			generate_source(project1, ('github', 'weblink1')),
			generate_source(project2, ('github', 'weblink2')),
			generate_source(project2, ('github', 'weblink3'))
		]
		assert project1.get_data_sources(database) == [
			Git   (sources[0], project1, 'git',    'gitlink1'),
			Git   (sources[1], project1, 'git',    'gitlink2'),
			GitHub(sources[2], project1, 'github', 'weblink1'),
		]
		assert project2.get_data_sources(database) == [
			GitHub(sources[3], project2, 'github', 'weblink2'),
			GitHub(sources[4], project2, 'github', 'weblink3'),
		]
		assert project3.get_data_sources(database) == list()

		generate_source(project3, ('irrelevant', 'content'))
		assert project3.get_data_sources(database) == list() # Use cached version

	def test_project_equality(
		self,
		missing_project: Project,
	) -> None:
		assert missing_project == Project(9999)
		assert missing_project == 9999
		assert missing_project == 'oup'
		assert missing_project != Project(9998)
		assert missing_project != 9998
		assert missing_project != 'ouch'
		assert missing_project != {'irrelevant': 'object'}

class TestUser:

	def test_user_constructor(self) -> None:
		u = User(1)

		# Uninitialized variables
		u._user_id == 1
		u._username == None
		u._projects == []

	def test_instantiate_invalid_user_id(self) -> None:
		with raises(MalformedId):
			User(-1)
		with raises(MalformedId):
			User(999999999999999999999999)

	def test_load_user(self, database: Cursor, user: User) -> None:
		user.load(database)
		assert user._username == "Emi"

	def test_load_nonexistant_user(self, database: Cursor) -> None:
		user = User(1312)
		with raises(NonexistantId):
			user.load(database)

	def test_implicit_load_user(self, database: Cursor, user: User) -> None:
		assert user.get_username(database) == "Emi"

	def test_check_owns(
		self,
		database: Cursor,
		user: User,
		projects: Callable[[str], Project],
		establish_ownership: Callable[[User, Project], None]
	) -> None:
		project1 = projects("Mystic")
		project2 = projects("Kochab")
		project3 = projects("Linux")
		establish_ownership(user, project1)
		establish_ownership(user, project2)

		assert user.check_owns(database, project1)
		assert user.check_owns(database, project2)
		assert not user.check_owns(database, project3)

	def test_delete_source_if_owned(
		self,
		user: User,
		projects: Callable[[str], Project],
		establish_ownership: Callable[[User, Project], None],
		generate_source: Callable[[Project, Tuple[str, str]], int],
		database: Cursor
	) -> None:
		project1 = projects("Mystic")
		project2 = projects("Kochab")
		project3 = projects("Linux")
		establish_ownership(user, project1)
		establish_ownership(user, project2)
		sources = [
			generate_source(project1, ('gitlab', 'https://opensource.ieee.org/rit/mystic')),
			generate_source(project1, ('git', 'https://opensource.ieee.org/rit/mystic.git')),
			generate_source(project1, ('git', 'https://opensource.ieee.org/rit/grimoirelab-perceval-osf.git')),
			generate_source(project2, ('gitea', 'https://fem.mint.lgbt/Emi/kochab')),
			generate_source(project2, ('git', 'http://fem.mint.lgbt/Emi/kochab.git')),
			generate_source(project3, ('git', 'https://git.kernel.org/')),
		]

		assert not user.delete_source_if_owned(database, sources[5])
		assert user.delete_source_if_owned(database, sources[3])
		assert user.delete_source_if_owned(database, sources[2])

		record_count = database.execute("""
			SELECT source_id FROM data_sources WHERE project_id IN (%s, %s, %s)
		""", (project1.project_id, project2.project_id, project3.project_id))
		assert record_count == 4
		source_set = {record[0] for record in database.fetchall()}
		assert source_set == {sources[0], sources[1], sources[4], sources[5]}

	def test_create_project(self, database: Cursor) -> None:
		u = User.create_user(database, 1312, "alch_emi", "Emi", "Simpson", "emi@mail.rit.edu")

		database.execute("""
			SELECT * FROM users WHERE user_id = %s;
		""", (u.user_id,))
		assert database.fetchone() == (1312, "alch_emi", "Emi", "Simpson", "emi@mail.rit.edu")

		database.execute("""
			DELETE FROM users WHERE user_id = %s;
		""", (u.user_id,))

	def test_lookup_user(self, database: Cursor, users: Callable[[str], User]) -> None:
		user1 = users('Martin')
		user2 = users('Faye')

		assert User.lookup_user(database, 'Martin') == user1
		assert User.lookup_user(database, 'Faye') == user2
		assert User.lookup_user(database, 'Dora') is None
		martin = User.lookup_user(database, 'Martin')
		assert martin is not None
		assert martin.get_username(database) == 'Martin'

	def test_projects(
		self,
		database: Cursor,
		users: Callable[[str], User],
		projects: Callable[[str], Project],
		establish_ownership: Callable[[User, Project], None]
	) -> None:
		project1 = projects('Tire Swing')
		project2 = projects('Deck')
		project3 = projects('Garden Planters')
		user1 = users('Pintsize')
		user2 = users('Raven')
		user3 = users('Emily')
		establish_ownership(user1, project1)
		establish_ownership(user1, project2)
		establish_ownership(user2, project2)
		establish_ownership(user2, project3)

		assert len(user1.get_projects(database)) == 2
		assert project1 in user1.get_projects(database)
		assert project2 in user1.get_projects(database)

		assert len(user2.get_projects(database)) == 2
		assert project2 in user2.get_projects(database)
		assert project3 in user2.get_projects(database)

		assert user3.get_projects(database) == []

		establish_ownership(user3, project3)
		assert user3.get_projects(database) == [] # Should use cache instead of repeating database query, expected behavior

	def test_user_equality(
		self,
	) -> None:
		user = User(621)
		assert user == User(621)
		assert user == 621
		assert user != User(926)
		assert user != 926
		assert user != "irrelevant object"

def test_compute_alphaid() -> None:
	assert compute_alphaid(0) == 'a'
	assert compute_alphaid(1) == 'b'
	assert compute_alphaid(26) == 'ba'
	assert compute_alphaid(26 ** 2 + 2) == 'bac'

	with raises(MalformedId):
		compute_alphaid(-1)

	with raises(MalformedId):
		compute_alphaid(-100)

def test_parse_alphaid() -> None:
	assert parse_alphaid('') == 0
	assert parse_alphaid('a') == 0
	assert parse_alphaid('aa') == 0
	assert parse_alphaid('b') == 1
	assert parse_alphaid('ab') == 1
	assert parse_alphaid('ba') == 26
	assert parse_alphaid('bac') == 26 ** 2 + 2
	assert parse_alphaid('acab') == 2 * 26 ** 2 + 1

	with raises(MalformedId):
		parse_alphaid('ACAB')

	with raises(MalformedId):
		parse_alphaid('100')

	with raises(MalformedId):
		parse_alphaid('-b')
