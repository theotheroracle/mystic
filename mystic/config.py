"""
Hosts a wide variety of configuration, setup, and boilerplate code.

Most of the tasks this module provides involve setting up the flask app object in some way
or another, for example by registering hooks and views, or by setting up Jinja global
variables.

Other tasks involve reading from the actual configuration file, and making it's content
available in a convenient way.

A more complete list of responsibilities:
 - Setting up and connecting to the databases
 - Making some simple python utilities available to Jinja templates
   - A utility for generating version strings to more effectively cache static assets
   - A utility for checking if the user has enabled high contrast mode
   - Exposing UUID and json utilities
   - Adding a list of accepted source types
 - Configuring the application hooks
 - Providing the SAML service provider object
"""
from __future__ import annotations
from base64 import b64encode
from hashlib import sha1
import json
from os import path, getcwd
from elasticsearch.client import Elasticsearch

import pymysql
from pymysql.connections import Connection
from pymysql.cursors import Cursor
from flask import Flask, url_for, current_app, session, g
from flask.wrappers import Response
from flask_saml2.sp import ServiceProvider
from typing import Any, Dict, Optional, cast
import uuid

from pymysql.err import OperationalError

from mystic import views
from mystic.init_database import setup_database
from mystic.sources import SOURCE_PROCESSORS

def is_high_contrast() -> bool:
	return cast(bool, session.get('high_contrast', False))

def get_database() -> Connection[Cursor]:
	"""
	Set up a connection to the database by referencing current app config

	Raises:
		OperationalError: The database config is invalid
	"""
	db: Connection[Cursor] = g.get('database', None)
	if db is None:
		db_config: Dict[str, str] = current_app.config['DATABASE']
		db = pymysql.connect(
			host = db_config['host'],
			user = db_config['user'],
			password = db_config['password'],
			database = db_config['database'],
			charset='utf8mb4',
			cursorclass=Cursor,
		)
		g.database = db
	return db

def get_elastic() -> Elasticsearch:
	# TODO load from configuration
	es = Elasticsearch(
		current_app.config['ELASTIC']['HOSTNAMES'],
		port=current_app.config['ELASTIC']['PORT'],
	)
	return es

file_hashes: Dict[str, str] = {}
def static_versioned(filename: str) -> str:
	"""
	Generate a path to a static file with a version string attached

	Version string is derived from the b64 encoded sha1 hash of the file, and are only
	computed once per file per run

	Very loosely derived from BSD licensed code at
	https://github.com/bmcculley/flask-autoversion/blob/master/flaskext/autoversion/__init__.py
	"""
	if filename not in file_hashes:
		print(f'[INFO] Hashing static file {filename}')
		assert isinstance(current_app.static_url_path, str)
		fullpath = path.join(
			cast(str, current_app.root_path),
			'static',
			filename
		)
		try:
			with open(fullpath, 'rb') as file:
				h = sha1()
				BUF_SIZE = 65536
				while True:
					buff = file.read(BUF_SIZE)
					if not buff:
						break
					h.update(buff)
		except OSError as err:
			print(f'WARN: {err}')
			return url_for('static', filename=filename)
		output = b64encode(h.digest(), altchars=b'+-').decode('UTF-8')[:8]
		file_hashes[filename] = output
	return url_for('static', filename=filename, v=file_hashes[filename])

def create_app(test_config: Optional[Dict[str, Any]] = None) -> Flask:
	app = Flask(__name__)

	app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 60 * 60 * 24 * 7 # One week

	if test_config is not None:
		app.config.from_mapping(test_config)
	else:
		import os
		if 'MYSTIC_CONFIG' in os.environ:
			config_path = os.environ['MYSTIC_CONFIG']
		else:
			config_path = 'config.cfg'
		app.config.from_pyfile(config_path)

	app.register_blueprint(views.bp)
	app.register_blueprint(service_provider.create_blueprint(), url_prefix='/saml/')
	app.jinja_env.globals.update(uuid=uuid.uuid4)
	app.jinja_env.globals.update(high_contrast=is_high_contrast)
	app.jinja_env.globals.update(json=json)
	app.jinja_env.globals.update(static_versioned=static_versioned)
	app.jinja_env.globals.update(valid_sources=[
		(source.display_name(), source_key)
		for (source_key, source)
		in SOURCE_PROCESSORS.items()
	])

	def configure_database() -> None:
		try:
			with get_database().cursor() as c:
				setup_database(c)
		except OperationalError as e:
			if len(e.args) > 1:
				message: str = e.args[1]
			else:
				message = e.args[0]
			print('FATAL: Failed to connect to the database.  ' + message)
			exit(1001)
	app.before_first_request(configure_database)

	def close_database(_: Optional[BaseException]) -> Response:
		db = getattr(g, 'database', None)
		if db is not None:
			db.close()
		return None #type: ignore
	app.teardown_appcontext(close_database)

	return app

class MysticServiceProvider(ServiceProvider):
	def get_default_login_return_url(self) -> str:
		return url_for('main.main')

	def get_logout_return_url(self) -> str:
		return url_for('main.main')

service_provider = MysticServiceProvider()
