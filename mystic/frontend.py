"""
Handle computation for incoming requests

The frontend module is responsible for doing the heavy lifting of request processing.
While the `mystic.views` module receives the requests and selects the feeds data into
templates, `mystic.frontend` is the real workhorse.

Most methods in this module specialize in parsing out arguments from forms or query
parameters, deciding what action needs to be taken, then either producing an error on
receiving an invalid request, or handing processing off to another module.

This module also includes methods like `get_user` and `get_project`, which can be used to
determine the active user/project.
"""
import json
from mystic.analytics.queries import QueryMachine
from mystic.analytics.dashboards import DASHBOARDS, Dashboard
from elasticsearch.client import Elasticsearch

from flask.helpers import flash
from pymysql.cursors import Cursor
from pymysql.err import IntegrityError
from mystic.sources import SOURCE_PROCESSORS
from flask import request, current_app, session, g
from mystic.database import MalformedId, Project, User
from mystic.config import service_provider
from mystic.projects import produce_project_report
from typing import Callable, Dict, List, Optional, cast

class ErrorTuple(Exception):
	"""
	An error meant to be sent to the user

	This consists of two parts:
	- a category, which indicates where the problem occured, and where it should be
	  displayed.
	- an error message, the actual content that should be displayed to the user.
	"""
	def __init__(self, category: str, message: str):
		self.category = category
		"""
		A string indicating where the error occured, or what component it occured with.

		This is often used to communicate what UI component the error message needs to be
		displayed alongside, or if it should be displayed as a global error message.  The
		exact meaning of this field often depends on the method throwing it, so check the
		docs!
		"""

		self.message  = message
		"""
		The actual content of the error to be displayed to the user

		All error messages are designed to be human-readable, but error messages that are
		likely to occur by a well-meaning user who simply input the wrong data are
		designed to be user-readable.  For example, a human-reabable error might be
		something like "project_id was not specified in request, but is required for
		edit_project", while a user-readable error might be something like "Please give
		your project a name"
		"""

def get_user(cursor: Cursor) -> Optional[User]:
	"""
	Retrieve the current user from the database/session data

	If the user is not currently logged in, returns None, otherwise, creates the user (if
	necessary) and returns them from the database.

	Raises:
		sqlite3.OperationalError:  The provided cursor that points to a database that was
			not properly set up
	"""
	if 'user' not in g:
		if not service_provider.is_user_logged_in():
			return None
		user_info = service_provider.get_auth_data_in_session().attributes
		g.user = User.create_or_update(
			cursor,
			int(user_info['uidNumber']),
			user_info['uid'],
			user_info['givenName'],
			user_info['sn'],
			user_info['mail'],
		)
	return cast(User, g.user)

def get_user_force_login(cursor: Cursor) -> User:
	"""
	Like :func:`get_user`, but instead of returning `None`, forces the user to log in
	"""
	service_provider.login_required()
	user = get_user(cursor)
	assert user is not None
	return user

def get_project(cursor: Cursor) -> Project:
	"""
	Retrive the project specified in the `request.form`

	This requires the user to log in, and also verifies that the user owns the project
	before returning.

	Raises:
		ErrorTuple: There was a problem with the user's input
		sqlite3.OperationalError:  The provided cursor that points to a database that was
			not properly set up
	"""
	if 'cursor' not in g:
		if 'id' not in request.form:
			raise ErrorTuple("generic", "No project id specified")
		try:
			user = get_user_force_login(cursor)
			g.project = next(
				project
				for project
				in user.get_projects(cursor) + [user.get_draft_project(cursor)]
				if project.alphaid == request.form['id']
			)
		except StopIteration:
			raise ErrorTuple("generic", "Either you don't own this project, or it doesn't exist")
	return cast(Project, g.project)

def subaction_add_source(cursor: Cursor) -> None:
	"""
	Add a data source to a project

	Requires that the following form fields be filled:
	 - source_type: The type of the datasource, one of :data:`Project.VALID_SOURCES`
	 - source_url:  The URL of the datasource to add
	 - id:   The alphaid of the project to add the datasource to
	"""
	project = get_project(cursor)
	try:
		source_type = request.form['source_type'].strip()
		source_url  = request.form['source_url'].strip()
	except KeyError:
		raise ErrorTuple(
			f"add_source",
			"Missing parameter, make sure both 'type' and 'url' are specified"
		)
	if len(source_url) == 0:
		raise ErrorTuple(
			f"add_source",
			"Make sure you include a URL for your data source"
		)
	elif source_type not in SOURCE_PROCESSORS:
		raise ErrorTuple(
			f"add_source",
			"Invalid source type specified"
		)
	source = SOURCE_PROCESSORS[source_type](-1, project, source_type, source_url)

	validation_error = source.validate_source()
	if validation_error is not None:
		raise ErrorTuple(
			"add_source",
			validation_error
		)

	try:
		project.add_data_source(cursor, source_type, source_url)
	except IntegrityError:
		raise ErrorTuple(
			f"add_source",
			"This data source has already been added"
		)
	_save_projects_json(cursor)

def subaction_add_owner(cursor: Cursor) -> None:
	"""
	Add an owner to a project

	Requires that the following form fields be filled:
	 - new_owner: The username of the owner to add
	 - id:        The alphaid of the project to add the owner to
	"""
	project = get_project(cursor)
	try:
		new_owner = User.lookup_user(cursor, request.form['new_owner'])
	except KeyError:
		raise ErrorTuple("add_owner", "The 'username' parameter must be specified")
	if new_owner is None:
		raise ErrorTuple("add_owner", "That user doesn't exist, or has never logged in")
	else:
		try:
			project.add_owner(cursor, new_owner)
		except IntegrityError:
			raise ErrorTuple("add_owner", "This user already owns this project")

def action_delete_source(cursor: Cursor) -> None:
	"""
	Delete a data source from a project

	Requires that the following form fields be filled:
	 - source_id: The numeric id of the source to remove
	"""
	if 'source_id' not in request.form:
		raise ErrorTuple('add_source', 'Must include a \"source_id\" to delete')
	try:
		source_id = int(request.form['source_id'])
	except ValueError:
		raise ErrorTuple("add_source", "Source ID is not in a valid integer format")
	if get_user_force_login(cursor).delete_source_if_owned(cursor, source_id):
		_save_projects_json(cursor)
	else:
		raise ErrorTuple(
			"add_source",
			"Either you don't own the project this source belongs to, or it doesn't exist"
		)

def action_remove_owner(cursor: Cursor) -> None:
	"""
	Remove an owner from a project

	Requires that the following form fields be filled:
	 - user: The user to be removed
	 - id:   The alphaid of the project to remove
	"""
	project = get_project(cursor)
	if 'user' not in request.form:
		raise ErrorTuple("add_owner", "Please include the 'user' field")
	try:
		owner = User.from_alphaid(request.form['user'])
	except MalformedId:
		raise ErrorTuple("add_owner", f"Malformed ID:  {request.form['user']}")
	if not project.remove_owner(cursor, owner):
		raise ErrorTuple(
			"add_owner",
			"Either the user, project, or ownership relationship didn't exist"
		)

def action_edit(cursor: Cursor) -> None:
	"""
	A metaaction for all actions submitted through the edit project menu

	The following subactions are available through this action:
	 - :func:`subaction_add_owner`:       If new_owner is defined and not empty
	 - :func:`subaction_add_source`:      If source_url is defined and not empty
	 - :func:`subaction_edit_properties`: If neither of the above are available

	See the individual subactions for required form fields

	Following the completion of one of the first two subactions, any values submitted for
	the project name and description are flashed to the next template under the categories
	`data-{projectid}-desc` and `data-{projectid}-name`.
	"""
	project = get_project(cursor)

	for field in ['name', 'desc']:
		if field in request.form:
			flash(request.form[field], f'data-{project.alphaid}-{field}')

	if len(request.form.get('new_owner', '').strip()) != 0:
		subaction_add_owner(cursor)
	elif len(request.form.get('source_url', '').strip()) != 0:
		subaction_add_source(cursor)
	else:
		subaction_edit_properties(cursor)
		return None

def subaction_edit_properties(cursor: Cursor) -> None:
	"""
	Edit a project, or move it from draft to a realized project

	Requires that the following form fields be filled:
	 - id:   The alphaid of the project to finalize
	 - name: [Optional] The new name of the project
	 - desc: [Optional] The new description of the project

	If the project is a draft, all fields are required.
	"""
	project = get_project(cursor)
	if project.is_draft(cursor):
		try:
			name = request.form['name'].strip()
			desc = request.form['desc'].strip()
		except KeyError:
			raise ErrorTuple(
				'generic',
				'New projects must have both "name" and "desc" specified.'
			)
		if len(name) == 0:
			raise ErrorTuple(
				'project-name',
				'What should we call your project?'
			)
		if len(desc) == 0:
			raise ErrorTuple(
				'description',
				'Tell us a little bit about your project'
			)
		project.finalize_draft(cursor, name, desc)
		_save_projects_json(cursor)
	else:
		needs_update = False
		if 'name' in request.form:
			project.set_display_name(request.form['name'])
			needs_update = True
		if 'desc' in request.form:
			project.set_description(request.form['desc'])
		project.save(cursor)
		if needs_update:
			_save_projects_json(cursor)

def action_delete_project(c: Cursor) -> None:
	"""
	Deletes a project

	Requires that the following form fields be filled:
	 - id:   The alphaid of the project to finalize
	"""

	project = get_project(c)
	project.delete(c)
	g.user._projects = None

def action_set_high_contrast(_: Cursor) -> None:
	"""
	Enables or disables high contrast mode

	Requires that the following form fields be filled:
	 - enabled: "True" if enabling high contrast mode, "False" if disabling it
	"""
	session['high_contrast'] = request.form.get("enabled") != 'False'

ACTIONS: Dict[str, Callable[[Cursor], None]]  = {
	"delete_source":     action_delete_source,
	"remove_owner":      action_remove_owner,
	"edit":              action_edit,
	"set_high_contrast": action_set_high_contrast,
	"delete_project":    action_delete_project,
}
"""
A table of available actions

This dict maps the name of the action to a callable to perform that action.  Each callable
takes a cursor pointing to the active database.  If the action succeeded, `None` is
returned.  However, if an error occured, an :class:`ErrorTuple` will be produced, although
categories will not be predicated with "error-{project_id}-" as with :func:`try_action`.
"""

def try_action(c: Cursor) -> None:
	"""
	Attempt to parse and perform an action from a :mod:`flask.request`

	This expects that the current context is a POST request, with a form containing an
	`action` field, as well as additional fields with arguments for that action.  For
	available actions and their arguments, see all of the `action_*` methods in this
	module.

	If successful, this method returns `None`.  Otherwise, an :class:`ErrorTuple` is
	raised.

	The error message component can take one of two forms.  If the error corresponds to a
	problem with a field of a specific project, the string will take the form of
	`"error-{project_id}-{component}"`.  For example, if a `name` field was missing, from
	an edit project dialog for project #8, the string might be `error-8-project_name`.  If
	no specific field was invalid, the `{component}` value will be `"generic"`.

	Alternatively, if there is no relevant project, the string will be
	`error-{component}`, or just `error-{generic}`.
	"""
	if "action" not in request.form:
		raise ErrorTuple("error-generic", "No action specified")
	action = request.form['action']

	if action not in ACTIONS:
		raise ErrorTuple("error-generic", f'Invalid action specified: "{action}"')
	action_method = ACTIONS[action]

	try:
		action_method(c)
	except ErrorTuple as e:
		if 'project' in g:
			project = cast(Project, g.project)
			e.category = f'error-{project.alphaid}-{e.category}'
		else:
			e.category = f'error-{e.category}'
		raise e

def get_analytics(c: Cursor, es: Elasticsearch, project: Project) -> List[Dashboard]:
	dashboards = [
		d
		for d in (
			d.produce_for(c, project)
			for d
			in DASHBOARDS
		)
		if d is not None
	]
	for d in dashboards:
		qm = QueryMachine()
		d.load(es, qm)
	return cast(List[Dashboard], dashboards)

def _save_projects_json(c: Cursor) -> None:
	report = produce_project_report(c)
	with open(cast(Dict[str, str], current_app.config)['PROJECTS_FILE'], 'w') as output_file:
		json.dump(report, output_file, indent=2)
