"""
Tools for source representation, validation, and expansion

Central to grimoirelab's function are sources, which are links to some foreign site that
can be scraped for useful information which in turn is used to produce metrics.  These
sources require both a type (eg. gitlab issues) and a url.  We represent sources as a
named tuple including:  An ID (for referencing the database), a project that the source
belongs too, the source type, and url.

Crucially, this module also provides validation and expansion for each source type.
Specifically, the :class:`Source` type is subclassed for each metatype of source.

To clarify, many sources require several entries in order to be processed by grimoirelab.
For example, a single gitlab repository might need three sources, a `gitlab:issue` to
compute metrics on the issues, a `gitlab:merge` to compute metrics on merge requests, and
a `git` to gather git-specific metrics.

In order to simplify the user's life, most of Mystic works in meta-sources.  This means
that the user can simply provide a `gitlab` source, and not worry about the particulars.
Source expansion is the process of taking that `gitlab` metasource and breaking it down
into it's component sources.

Note that the code doesn't really make a distinction between meta-sources and component
sources, other than the fact that component sources typically are of the base `Source`
type, while meta-sources more often have a subclass, like `GitLab`.  However, for sources
that don't require subtypes, like `rss`, the subclass is used both for the component and
meta-sources.  The real reason most component sources don't have a subclass is simply
because it isn't worth adding custom validation or expansion code, since it wouldn't ever
be used.

Validation is performed on meta-sources in order to check that they are valid.  Typically,
this just means matching the URL against a Regex.
"""
from mystic import database
import re

from typing import Any, Dict, List, NamedTuple, Optional, Type

URL_REGEX = re.compile(r'https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()!@:%_\+.~#?&\/\/=]*)$')

class Source(NamedTuple):
	"""
	A datasource belonging to a certain project.

	Please read the module-level docs for more information.  :mod:`mystic.sources`
	"""

	origin_id: int
	"""
	The ID of the source in the database that this originated from

	During the processing of sources, it's common for sources to expand.  For example, a
	GitLab source might expand into a 'gitlab:merge', 'gitlab:issue', and 'git' source.
	The origin ID field tracks what the original source was.  In the above example, all
	resulting sources will still share the same ID as the original source.

	This value also represents the entry in the database that this source corresponds to.

	If this source has yet to be added to the database, for example, if it is still being
	validated, then use -1 for the origin_id.
	"""

	project: Any
	"""
	The project this source belongs to
	"""

	source_type: str
	"""
	The type of the source, a short string matching `sirmordred`'s `projects.json` format
	"""

	url: str
	"""
	The URL of the resource to be scraped by this source
	"""

	def expand_source(self) -> List['Source']:
		"""
		Given a source, expand it to any component sources

		Some sources, like github, need to be expanded into component sources, like
		github:issue, github:pull, and git.  This method allows for this by producing a
		series of sources derived from this source, including the source itself, if
		necessary.

		The default implementations does not add any additional sources.
		"""
		return [self]

	def validate_source(self) -> Optional[str]:
		"""
		Attempt to validate the form of the source.

		Return `None` if valid, otherwise return a human-readable string detailing why the
		source couldn't be validated.  If validating with a regex, this can be as simple
		as, "Source URL must match the form {some_form}".

		This request MAY make external calls to the API to validate the source, but is not
		required to do so.  However, note that doing so will mean that the user gets more
		immediate feedback on the validity of their URL

		The default behavior is not to perform validation, although it is recommended that
		implementors override this.
		"""
		return None

	def get_simplified_form(self) -> str:
		"""
		Produces a shorter representation of this source

		Often, sources take the form of long and difficult to read urls, like
		https://gitlab.com/open-rit/mystic.  This method should produce a shorter string
		that still conveys what the source is, given the context of the source type.  For
		example, the previous link could be shortened to open-rit/mystic, since a user
		could extrapelate the full url from that, given that they knew it was a gitlab
		link.

		The default behavior is to return the URL with the protocol stripped, but it is
		strongly recommended to override this.
		"""
		return self.url.lstrip('http:').lstrip('https:').strip('/')

	@staticmethod
	def display_name() -> str:
		raise NotImplementedError()

class Git(Source):
	@staticmethod
	def display_name() -> str:
		return 'Generic Git Repository'

class GitHub(Source):
	URL_REGEX = re.compile(r'https://github\.com/[\w\-]+/[\w\-]+/?$')

	def expand_source(self) -> List[Source]:
		return [
			Source(self.origin_id, self.project, self.source_type, self.url),
		] + Git(self.origin_id, self.project, 'git', self.url.rstrip('/') + '.git').expand_source()
		#TODO GitHub2 analytics (https://github.com/chaoss/grimoirelab-sirmordred#github2-)

	def validate_source(self) -> Optional[str]:
		if GitHub.URL_REGEX.match(self.url) is None:
			return 'GitHub URLs should be in the form "https://github.com/USER/PROJECT"'
		else:
			return None

	def get_simplified_form(self) -> str:
		"""
		Returns the USER/PROJECT part of the URL
		"""
		return self.url[19:].strip('/')

	@staticmethod
	def display_name() -> str:
		return 'GitHub'

class GitLab(Source):
	URL_REGEX = re.compile(r'https://gitlab\.com/[\w\-]+/[\w\-]+/?')
	def expand_source(self) -> List[Source]:
		return [
			Source(self.origin_id, self.project, self.source_type + ':issue', self.url),
			Source(self.origin_id, self.project, self.source_type + ':merge',  self.url),
			] + Git(self.origin_id, self.project, 'git', self.url.rstrip('/') + '.git').expand_source()

	def validate_source(self) -> Optional[str]:
		if GitLab.URL_REGEX.match(self.url) is None:
			return 'GitLab URLs should be in the form "https://gitlab.com/USER/PROJECT"'
		else:
			return None

	def get_simplified_form(self) -> str:
		"""
		Returns the USER/PROJECT part of the URL
		"""
		return self.url[19:].strip('/')

	@staticmethod
	def display_name() -> str:
		return 'GitLab'

class Rss(Source):
	@staticmethod
	def display_name() -> str:
		return 'RSS Feed'

	def validate_source(self) -> Optional[str]:
		if URL_REGEX.match(self.url) is None:
			return 'RSS feeds must be a valid URL.  Be sure to include the https:// at the start!'
		else:
			return None

SOURCE_PROCESSORS: Dict[str, Type[Source]] = {
	'git':    Git,
	'github': GitHub,
	'gitlab': GitLab,
	'rss':    Rss,
	#TODO Add more sources (https://github.com/chaoss/grimoirelab-sirmordred#supported-data-sources-)
}
"""
A list of available source types, and their corresponding `Source` subclasses

This is most commonly used when processing metasources.  Often, you will have access to a
project id, source type, and a url, and nothing more.  This dict is made available to
create a `Source` object of the appropriate subclass, or just to validate that a source
type is valid.

The results of this dict can be used as constructors to construct a source.

## Example
```python
source_project = Project(-1) # Pretend this is a real project
source_type = 'gitlab'
source_url = 'https://gitlab.com/open-rit/mystic'

# -1 is used for the ID, as there is no associated id yet
source = SOURCE_PROCESSORS[source_type](-1, project, source_type, source_url)
```
"""
