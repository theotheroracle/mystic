"""
Handles interaction with elasticsearch

This module is broken into several queries, each which returns some aggregation of data.
Each query is an implementation of the generic `Query` class, with the type variables `In`
and `Out` overriden to specify what arguments the query accepts and what format the
returned data will be in.

This module also contains the `QueryMachine` class, which is responsible for being a
caching intermediary for the queries.This was originally implented assuming that there
might be some dashboards which re-use queries
"""
from abc import ABC
from typing import Any, Dict, Generic, List, Type, TypeVar, TypedDict

from flask import json

from elasticsearch.client import Elasticsearch


class _QueryHit(TypedDict):
	_index: str
	_type: str
	_id: str
	_score: float
	_source: Dict[str, Any]

class _QueryHits(TypedDict):
	total: int
	max_score: float
	hits: List[_QueryHit]

class _QueryResult(TypedDict):
	took: int
	timed_out: bool
	_shards: Dict[str, int]
	hits: _QueryHits
	aggregations: Dict[str, Any]

class _DateHistogramAggBucket(TypedDict):
	key_as_string: str
	key: int
	doc_count: int

class _DateHistogramAgg(TypedDict):
	buckets: List[_DateHistogramAggBucket]

class _HistogramAggBucket(TypedDict):
	key: str
	doc_count: int

class _HistogramAgg(TypedDict):
	buckets: List[_HistogramAggBucket]

class _TermsAggBucket(TypedDict):
	key: str
	doc_count: int

class _TermsAgg(TypedDict):
	doc_count_error_upper_bound: int
	sum_other_doc_count: int
	buckets: List[_TermsAggBucket]

In = TypeVar('In')
Out = TypeVar('Out')
class Query(ABC, Generic[In, Out]):
	"""
	Represents a potentially reusable elasticsearch query

	Used to generate dashboards
	"""
	@staticmethod
	def perform_query(es: Elasticsearch, args: In) -> Out:
		"""
		Run the query this class describes

		This should run one or more elasticsearch queries in order to retrieve the data
		this :class:`Query` describes.  Any necessary information, such as the project the
		query is being performed for, should be requested through the `In` type variable.
		No SQL queries should be performed, and all processing of data, both input and
		output, should be left to the caller.  This should ONLY perform the elastic query
		itself.

		No attempt should be made to cache this data, as this will be handled by the
		:class:`QueryMachine`.
		"""
		raise NotImplementedError

class QueryMachine:
	"""
	An caching intermediary between :class:`Dashboard`s and :class:`Query`s

	This was originally implemented in anticipation that some dashboards might re-use a
	query and display different parts of it in different ways.  This has yet to happen,
	however, although this class will likely remain for the time being.  It's worth noting
	that this class has the same API as if the `Query` were used directly, so removing it
	is pretty trivial.
	"""
	def __init__(self):
		self.cache: Dict[Type[Query[Any, Any]], Any] = dict()

	def perform_query(self, es: Elasticsearch, query: Type[Query[In, Out]], args: In) -> Out:
		"""
		Perform the provided query, or retrieve a cached value
		"""
		if query in self.cache:
			out = self.cache[query]
			assert type(out) == Out
			return out
		else:
			out = query.perform_query(es, args)
			self.cache[query] = out
			return out

class CommitsByAuthor(Query[str, Dict[str, int]]):
	"""
	Get commits for a project grouped by commit author

	Input should be the project name, and output is a mapping from author name to the
	number of commits made by that author.
	"""
	@staticmethod
	def perform_query(es: Elasticsearch, args: str) -> Dict[str, int]:
		project_name = args

		results: _QueryResult = es.search(index="git_demo_enriched", body = {
			'size': 0,
			"query": {
				"bool": {
					"must": [
						{
							"match": {"project": project_name}
						}
					]
				}
			},
			'_source': ['author_name'],
			'aggs': {
				'group_by_author': {
					'terms': {
						'field': 'author_name',
					}
				}
			}
		})

		agg: _TermsAgg = results['aggregations']['group_by_author']
		buckets = agg['buckets']

		return dict(
			(b['key'], b['doc_count'])
			for b in buckets
		)

class CommitsByDate(Query[str, Dict[str, int]]):
	"""
	Get a commit histogram by date

	Input should be a project name, and output is a mapping from a date to a count
	"""
	@staticmethod
	def perform_query(es: Elasticsearch, args: str) -> Dict[str, int]:
		project_name = args
		results: _QueryResult = es.search(index="git_demo_enriched", body = {
			'size': 0,
			"query": {
				"bool": {
					"must": [
						{
							"match": {"project": project_name}
						}
					]
				}
			},
			'aggs': {
				'by_date': {
					'auto_date_histogram': {
						'field': 'author_date',
						'buckets': 30,
						'format' : 'yyyy-MM-dd',
					}
				}
			}
		})
		agg: _DateHistogramAgg = results['aggregations']['by_date']
		buckets = agg['buckets']

		return dict(
			(b['key_as_string'], b['doc_count'])
			for b in buckets
		)

class IssuesByOpenTime(Query[str, Dict[float, int]]):
	"""
	Get histogram of issues grouped by the amount of time they were open before closing

	Input should be a project name, and output is a mapping from a date to a count

	Looks at both github and gitlab issues.
	"""
	@staticmethod
	def perform_query(es: Elasticsearch, args: str) -> Dict[float, int]:
		project_name = args
		DEGREE: int = 2
		results: _QueryResult = es.search(index="github_issues,gitlab", body = {
			'size': 3,
			"query": {
				"bool": {
					"must": [
						{
							"match": {"project": project_name}
						}
					]
				}
			},
			'script_fields': {
				'time_open_log': {
					'script': {
						'lang': 'expression',
						'source': 'ln(doc["time_open_days"]) / ln(degree)',
						'params': {
							'degree': DEGREE,
						}
					}
				}
			},
			'aggs': {
				'by_open_time': {
					'histogram': {
						'field': 'time_open_days',
						'interval': 1,
						'script': {
							'lang': 'expression',
							'source': 'ln(_value) / ln(degree)',
							'params': {
								'degree': DEGREE,
							}
						}
					}
				}
			}
		})
		print(json.dumps(results, indent=2))
		agg: _HistogramAgg = results['aggregations']['by_open_time']
		buckets = agg['buckets']

		return dict(
			(DEGREE ** int(b['key']), b['doc_count'])
			for b in buckets
		)
