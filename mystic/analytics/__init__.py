"""
The analytics module runs analytics on projects, and renders those analytics into pretty
dashboards.  The module is broken into two submodules, each responsible for one of those
tasks.

`mystic.analytics.queries` is resposible for running elasticsearch queries on projects,
while `mystic.analytics.dashboards` renders those analytics into chart.js chart
configuration objects.

The typical flow through this code is:
 - An incoming request requests metrics for a project
 - The server runs through a list of dashboards from `mystic.analytics.dashboards`, and
   determines which ones are viable for the project.
 - Each viable dashboard is then loaded by calling
   `mystic.analytics.dashboards.Dashboard.load`, which in turn calls
   `mystic.analytics.queries.Query.perform_query`, which returns the requested
   aggregations to the dashboard.
 - The dashboard produces a chart configuration, which is then passed to a Jinja template.
 - The template with the chart configuration is returned to the user, where it is
   rendered.
"""
