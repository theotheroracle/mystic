"""
Produces visualizations of data aggregations through chart.js

The dashboards module is home to many different dashboard widgets.  Each widget inherits
from the parent `Dashboard` class, with a custom implementation for specifying what data
the dashboard needs, and rendering that data to a chart configuration object.
"""
from abc import ABC
from hashlib import sha1
from mystic.analytics.queries import CommitsByAuthor, CommitsByDate, IssuesByOpenTime, QueryMachine
from typing import Any, Dict, List, Optional, Set, TypedDict, Union

from pymysql.cursors import Cursor
from mystic.database import Project
from elasticsearch.client import Elasticsearch


class _ChartData(TypedDict):
	"""
	https://www.chartjs.org/docs/latest/api/interfaces/chartdata.html
	"""
	datasets: List[Dict[str, Any]]
	labels: List[str]

class _ChartConfiguration(TypedDict):
	"""
	https://www.chartjs.org/docs/latest/api/interfaces/chartconfiguration.html
	"""
	data: _ChartData
	options: Optional[Dict[str, Any]]
	type: str

class Dashboard(ABC):
	"""
	A potential visualization of a metric

	This includes the project that the metric is being calculated for, relevant data
	retrieved from ElasticSearch (if loaded), and code for rendering this to HTML.
	"""

	@staticmethod
	def produce_for(c: Cursor, project: Project) -> Optional['Dashboard']:
		"""
		Produce a dashboard for a given project

		This should check if the project is valid for this dashboard type (returning
		`None` if it isn't), but should not perform any more queries than is necessary for
		this stage, and should definately not make any elasticsearch queries, perform any
		IO, or in any other way try to compute any metrics.
		"""
		raise NotImplementedError

	@staticmethod
	def _validate_source_types(c: Cursor, project: Project, required_types: Set[str]) -> bool:
		"""
		Validate that the provided project has one of the required sources

		This method SHOULD NOT be overridden by implementors.

		Validate that this project's expanded sources include at least one of the provided
		sources.  The `required_types` argument should be a list of strings representing
		the expanded source types that can be used.

		For example, a dashboard wanting to perform analytics on git commits should pass
		`{'git'}`.  This function would then return `True` if the project had a `Git`
		source, a `GitLab` source, or a `GitHub` source, since all three of these source
		types expand to include a `git` source.

		Alternatively, a dashboard wanting to do analytics on issues may use
		`{'gitlab:issues', 'github'}` to match any projects that have either a github or
		gitlab linked.

		It's recommended to use this method in the implementation of :func:`produce_for`
		in order to validate a project.
		"""
		return any(
			True
			for parent_source in project.get_data_sources(c)
			for source in parent_source.expand_source()
			if source.source_type in required_types
		)

	def load(self, es: Elasticsearch, query_machine: QueryMachine) -> None:
		"""
		Perform all necessary queries to generate necessary statistics.

		All queries should be performed by the :class:`QueryMachine` to benefit from
		caching.  When implementing new dashboards, attempt to extend
		:class:`QueryMachine` in order to add new metrics, prefering to add new fields to
		existing queries over adding new queries entirely.

		This method should simply retrieve the necessary queries from the query machine
		and store them.
		"""
		raise NotImplementedError

	def render(self) -> _ChartConfiguration:
		"""
		Render the dashboard to a charts.js chart configuration

		This should, without performing any IO, render a dict suitable to be fed to the
		charts.js `new Chart()` constructor

		See https://www.chartjs.org/docs/latest/api/interfaces/chartconfiguration.html
		"""
		raise NotImplementedError

	def title(self) -> str:
		"""
		Determine the title of the dashboard

		Returns the title for the dashboard.  This is often fixed per type, such as
		"Commit frequency", but may depend on the data if need be
		"""
		raise NotImplementedError

	def is_tall(self) -> bool:
		"""
		Allows a dashboard to request extra vertical space

		Doubles the amount of vertical space available to a dashboard.  Most dashboards
		are expected to be roughly landscape shaped, but some may be more square.  These
		dashboards can request extra space by returning `True` here.

		The default behaviour is to return `False`.  This does not have to be overriden
		unless the dashboard is tall.
		"""
		return False

class CommittersDashboard(Dashboard):

	def __init__(self, project_name: str):
		self.project_name = project_name
		self.retrieved_data: Optional[Dict[str, int]] = None

	@staticmethod
	def produce_for(c: Cursor, project: Project) -> Optional['CommittersDashboard']:
		if Dashboard._validate_source_types(c, project, {'git'}):
			return CommittersDashboard(project.get_display_name(c))
		else:
			return None

	def load(self, es: Elasticsearch, query_machine: QueryMachine) -> None:
		self.retrieved_data = query_machine.perform_query(es, CommitsByAuthor, self.project_name)

	def render(self) -> _ChartConfiguration:
		assert self.retrieved_data is not None, ".load() must be called before .render()"
		return {
			'type': 'doughnut',
			'data': {
				'labels': list(self.retrieved_data.keys()),
				'datasets': [
					{
						'label': self.title(),
						'data': list(self.retrieved_data.values()),
						'backgroundColor': [
							color_for_name(name)
							for name in self.retrieved_data.keys()
						]
					}
				]
			},
			'options': None
		}

	def title(self) -> str:
		return "Contributors"

	def is_tall(self) -> bool:
		return True

class CommitsDashboard(Dashboard):

	def __init__(self, project_name: str):
		self.project_name = project_name
		self.retrieved_data: Optional[Dict[str, int]] = None

	@staticmethod
	def produce_for(c: Cursor, project: Project) -> Optional['CommitsDashboard']:
		if Dashboard._validate_source_types(c, project, {'git'}):
			return CommitsDashboard(project.get_display_name(c))
		else:
			return None

	def load(self, es: Elasticsearch, query_machine: QueryMachine) -> None:
		self.retrieved_data = query_machine.perform_query(es, CommitsByDate, self.project_name)

	def render(self) -> _ChartConfiguration:
		assert self.retrieved_data is not None, ".load() must be called before .render()"
		return {
			'type': 'line',
			'data': {
				'labels': list(self.retrieved_data.keys()),
				'datasets': [
					{
						'label': self.title(),
						'data': list(self.retrieved_data.values()),
						'fill': True,
						'borderColor': '#F76902',
						'tension': 0.2,
					}
				]
			},
			'options': {
				'scales': {
					'y': {
						'title': {
							'text': 'Commits',
							'display': True,
						}
					}
				}
			}
		}

	def title(self) -> str:
		return "Activity"

class IssuesDashboard(Dashboard):

	def __init__(self, project_name: str):
		self.project_name = project_name
		self.retrieved_data: Optional[Dict[float, int]] = None

	@staticmethod
	def produce_for(c: Cursor, project: Project) -> Optional['IssuesDashboard']:
		if Dashboard._validate_source_types(c, project, {'gitlab:issue', 'github'}):
			return IssuesDashboard(project.get_display_name(c))
		else:
			return None

	def load(self, es: Elasticsearch, query_machine: QueryMachine) -> None:
		self.retrieved_data = query_machine.perform_query(es, IssuesByOpenTime, self.project_name)

	def render(self) -> _ChartConfiguration:
		assert self.retrieved_data is not None, ".load() must be called before .render()"
		return {
			'type': 'bar',
			'data': {
				'labels': list(
					human_duration(duration)
					for duration in self.retrieved_data.keys()
				),
				'datasets': [
					{
						'label': self.title(),
						'data': list(self.retrieved_data.values()),
						'backgroundColor': '#F76902',
					}
				]
			},
			'options': {
				'indexAxis': 'y',
				'plugins': {
					'legend': {
						'position': 'right',
					}
				},
				'scales': {
					'y': {
						'title': {
							'text': 'Time Open',
							'display': True,
						}
					},
					'x': {
						'title': {
							'text': '# of issues',
							'display': True,
						}
					}
				}
			}
		}

	def title(self) -> str:
		return "Issue Open Durations"

DASHBOARDS = [
	CommittersDashboard,
	CommitsDashboard,
	IssuesDashboard,
]

### Utils ###

def color_for_name(name: str) -> str:
	"""
	Derive a unique color for a given string, with the specified lightness and value
	"""
	h = sha1(name.encode('utf-8')).digest()
	value: float = h[0] * h[1] * 365 / (255 * 255)
	return f'hsl({value:.2f}, 80%, 60%)'

TIME_DURATION_UNITS: Dict[float, str] = {
	1 * 365         : 'years',
	1 * 30          : 'months',
	1               : 'days',
	1 / 24          : 'hours',
	1 / 24 / 60     : 'minutes',
	1 / 24 / 60 / 60: 'seconds',
}
def human_duration(days: float) -> str:
	for (timespan, unit) in TIME_DURATION_UNITS.items():
		if days > timespan or unit == 'seconds':
			time_in_unit = days / timespan
			time_formatted = f'{time_in_unit:.1f}'
			if time_formatted[-1] == '0':
				return f'{time_formatted[:-2]} {unit}'
			else:
				return f'{time_formatted} {unit}'
	raise RuntimeError('Unreachable')
