'''
The views module is responsible for connecting Jinja templates to the code that they need.

See individual views below for details on exactly what templates are used, and what they
connect up with.
'''
from functools import reduce
from elasticsearch.client import Elasticsearch
from werkzeug.exceptions import BadRequest, NotFound
from mystic.database import MalformedId, NonexistantId, Project
from flask import json, render_template, Blueprint
from typing import Any, Dict, Tuple, cast

from mystic.config import get_database, get_elastic, service_provider
from mystic.frontend import get_analytics, try_action, ErrorTuple, get_user
import flask

bp = Blueprint("main", __name__, url_prefix="/")

@bp.route("/")
def main() -> str:
	'''
	Render the main page

	This is a view-only template that produces a main page.  Depending on if the user is
	logged in, this displays either the generic login page, or the projects page.  If the
	user has no projects, this renders `projects-empty.html` instead of `projects.html`.

	Other than retrieving information about the user, this view is pretty much self
	contained.

	This pages should never return an error message.
	'''
	db = get_database()
	with db.cursor() as cursor:
		user = get_user(cursor)
		if user is not None:
			user.get_draft_project(cursor) # Force creation of draft
			db.commit()
			if len(user.get_projects(cursor)) != 0:
				return render_template(
					"projects.html",
					login=_login_or_profile(),
					user=user, cursor=cursor
				)
			else:
				return render_template(
					"projects-empty.html",
					login=_login_or_profile(),
					user=user, cursor=cursor
				)
		else:
			return render_template(
				"landing.html",
				login=_login_or_profile(),
				cursor=cursor
			)

@bp.route("/<pid>")
def view_project(pid: str) -> str:
	"""
	Render a statistics page for a given project.

	The specific project the page is rendered for is determined by the [alphaid] in the
	URL.  If this url is malformed or nonexistant, this will be a 404.

	Otherwise, this view requests dashboards from `mystic.frontend.get_analytics` and
	renders them into the `project.html` template.

	[alphaid]: database.html#Project.alphaid
	"""
	db = get_database()
	with db.cursor() as cursor:
		user = get_user(cursor)
		try:
			project = Project.from_alphaid(pid)
			project.load(cursor)
		except MalformedId:
			#TODO:  Use actual error pages here
			raise NotFound
		except NonexistantId:
			raise NotFound

		es = get_elastic()

		dashboards = get_analytics(cursor, es, project)

		return render_template(
			"project.html",
			project=project,
			login=_login_or_profile(),
			user=user, cursor=cursor,
			dashboards=dashboards
		)

@bp.post("/")
def post_main() -> str:
	"""
	Change a setting, edit a project, or add something.

	This is a catch-all endpoint for changes that the user needs to make from the main
	page.  By sending a form with the `action` attribute set, any number of actions can be
	performed.

	This view calls `mystic.frontend.try_action` before flashing any error/success
	messages and  delegating the actual rendering to `main`.
	"""
	try:
		db = get_database()
		with db.cursor() as cursor:
			try_action(cursor)
			db.commit()
	except ErrorTuple as e:
		flask.flash(e.message, e.category)
	return cast(str, main())

def _login_or_profile() -> Tuple[str, str]:
	if service_provider.is_user_logged_in():
		name = service_provider.get_auth_data_in_session().attributes['givenName']
		return (name, '/account')
	else:
		return ('Login', service_provider.get_login_url())
