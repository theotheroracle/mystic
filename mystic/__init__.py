r'''
# Welcome to Mystic!

This will be a developer's overview of the architecture of Mystic.  If you're here looking
for documentation on how to set up mystic, try looking in the
[`README.md`][1] instead.  If you're looking for information about what mystic is, check
out our homepage at [foss-dashboard.rit.edu][2].  Detailed usage instructions have yet to
be written, but if anything isn't clear, you are encouraged to ask online by making a post
on our [issue tracker][3].  If you're still here, that means that you're a developer
looking for information about how Mystic's code is structured.  Perfect!  Let's get
started.

[TODO] Seperate out the configuration and setup part of the readme, and include here

[TODO] Add an introduction to terms, especially grimoirelab components

## Architecture

Mystic's architecture can roughly be seperated into about four layers:

- A **presentation layer**, consisting of assorted Jinja templates and some connective
  python, is responsible for the actual look and feel of the application, as well as
  determining how, where, and what data is displayed.  This is where flask requests enter
  the application.<br/>
  Key Modules: `mystic.views`, all the Jinja templates
- An **application layer** is contacted by the presentation layer, and is responsible for
  running the actual computations required by the request.  While the presentation layer
  might do some very light proccessing to get data into a format it's able to display, the
  application layer is responsible for doing the work to produce and transform that
  data.<br/>
  Key Modules: `mystic.frontend`, `mystic.analytics.dashboards`
- The **database layer** is responsible for being the interface between the application
  layer and the actual raw data that underlies the app, as well as persisting information
  about the users and projects.  Because Mystic uses both MySQL (For persisting it's own
  information) and ElasticSearch (Which is where Grimoirelab exports its data), this
  layer can fairly be split into two sublayers:
  - **The model** is the interface for the program's own data.  The model models higher
	level constructs like [`User`](mystic/database.html#User)s and
	[`Project`](mystic/database#Project)s, as well as any useful transformations that
	might need to be applied, like deleting a source from a project, or changing a User's
	name<br/>
    Key Modules: `mystic.database`, `mystic.sources`, `mystic.init_database`
  - **The query interface** is responsible for performing all the interesting data
	aggregations on data produced by grimoirelab.  This is pretty much just used by
	dashboards in order to create cool statistics to render.<br/>
	Key Modules: `mystic.analytics.queries`

For a detailed look at some key modules and how they interact, please see the .dot graph
in `docs/architecture/callgraph.dot` (displayed below).  Dotted lines indicate a reference
to a variable stored in a module that is accessed without running any actual code from
that module.

[TODO] Automatically render the graph.  For now, a static version is linked through [this
very professional CDN][3.5]

![Callgraph][3.5]

[1]: https://gitlab.com/open-rit/mystic/-/blob/main/README.md "Mystic README"
[2]: https://foss-dashboard.rit.edu/ "Mystic Homepage"
[3]: https://gitlab.com/open-rit/mystic/-/issues/new?issue "Mystic Issue Tracker"
[3.5]: https://cdn.discordapp.com/attachments/450749288834662410/864626965080047628/callgraph.png
'''
from mystic import views
from mystic.init_database import setup_database
from mystic.config import create_app
from mystic.frontend import try_action, ErrorTuple, get_project, get_user, get_user_force_login
